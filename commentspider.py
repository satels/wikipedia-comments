from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
from w3lib.html import remove_tags
from urllib.parse import urlencode
import re
import scrapy



date_pattern = re.compile(r"\s+\d{1,2}:\d{1,2},\s+\d{1,2}\s+[A-Z][a-z]+\s+\d{4}")


class Comment(scrapy.Item):
    comment = scrapy.Field()


class CommentSpider(CrawlSpider):
    name = 'wikipedia_comments'
    allowed_domains = [
        'en.wikipedia.org',
    ]

    rules = (
        Rule(LinkExtractor(allow=('Special%3AListUsers',))),
        Rule(LinkExtractor(allow=('Special%3AContributions',))),
    )

    def start_requests(self):
        yield scrapy.Request(url='https://en.wikipedia.org/w/index.php?title=Special%3AListUsers&offset=&limit=5000', callback=self.parse_list_users)

    def parse_list_users(self, response):
        for user_container in response.xpath('//a[@class="mw-userlink"]/bdi'):
            username = user_container.xpath('text()').extract_first()
            base_params = {
                'title': 'Special:Contributions',
                'contribs': 'user',
                'target': username,
                'limit': '5000',
            }
            yield scrapy.Request(url="https://en.wikipedia.org/w/index.php?" + urlencode(dict(base_params, namespace='3')),
                                 callback=self.parse_change_list)  # User talk
            yield scrapy.Request(url="https://en.wikipedia.org/w/index.php?" + urlencode(dict(base_params, namespace='1')),
                                 callback=self.parse_change_list)  # Talk
        next_urls = response.xpath('//a[@class="mw-nextlink"]/@href').extract()
        if next_urls:
            next_url = next_urls[0]
            yield scrapy.Request(url='https://en.wikipedia.org' + next_url, callback=self.parse_list_users)  # Next
        else:
            self.logger.error('Dnot found next url on: {}'.format(response.request.url))

    def parse_change_list(self, response):
        for url in response.xpath('//a[@class="mw-changeslist-date"]/@href').extract():
            yield scrapy.Request(url='https://en.wikipedia.org' + url, callback=self.parse_talk)  # User talk

    def parse_talk(self, response):
        for comment in response.xpath('//*[self::p or self::dd][descendant-or-self::text()]').extract():
            if not date_pattern.search(comment):
                continue
            item = Comment()
            item['comment'] = remove_tags(comment)
            yield item